---
title: g++编译时查看所有include的层次
date: 2018-03-23
categories: [实用命令]
tags: [编译]
---

# 命令如下
```bash
g++ -H cpp_file_name
```
**_NOTE:_** <code>-H</code>参数应该时Hierachy的意思,<code>-M</code>也可以打印出包含的所有的文件但是不能显示出层次结构
