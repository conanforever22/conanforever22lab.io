---
title: 查看编译器的默认include路径
date: 2018-03-23
categories: [实用命令]
tags: [编译]
---

# 命令如下
```bash
echo | gcc -E -Wp,-v -
```
