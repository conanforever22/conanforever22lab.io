---
title: 如何找到Linux某个系统调用函数的具体实现
date: 2018-06-25 10:24:22
categories: [实用命令]
tags: [Kernel, C]
---

# 下载Linux内核源码
系统调用是在Linux内核中实现的, 首先下载Linux内核源码, 比如下载Linux4.0内核源码使用下边的命令:
```bash
wget https://mirrors.edge.kernel.org/pub/linux/kernel/v4.x/linux-4.0.tar.gz
```
*NOTE:* 其他版本的内核可以到[Linux内核官网](https://mirrors.edge.kernel.org/pub/linux/kernel/)来下载

# 使用下边的命令到源码中进行查找某个系统调用的具体实现
```bash
grep -rn 'SYSCALL_DEFINE' . | grep '\.c' | grep 'getppid'
```

# 其他
如果[Linux内核官网](https://mirrors.edge.kernel.org/pub/linux/kernel/)下载速度较慢的话可以到[清华大学开源镜像站](https://mirrors.tuna.tsinghua.edu.cn/kernel/)来下载
