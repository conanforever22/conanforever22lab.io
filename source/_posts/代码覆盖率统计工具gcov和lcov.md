---
title: 单元测试覆盖率工具gcov/lcov学习笔记
date: 2018-05-10
categories: [工具]
tags: [单元测试, 覆盖率]
---

## gcov简介
* [CSDN参考博客](https://blog.csdn.net/livelylittlefish/article/details/6321861)
* `man gcov` 详细教程
* _gcov_ = **G**CC **Cov**erage
* 因为**_gcov_**是GCC的工具之一，所以需要配合**_gcc_**/**_g++_**
* 要求在编译的时候加上下边的编译选项
```bash
-g -O0 –coverage 
# -coverge 相当于-fprofile-arcs -ftest-coverage -lgcov
```
## gcov示例
* 示例代码
```cpp
#include <stdio.h>

int main(void)
{
    int i, total;

    total = 0;

    for (i = 0; i < 10; i++)
        total += i;

    if (total != 45)
        printf("Failure\n");
    else
        printf("Success\n");

    return 0;
}
```

* 编译
```bash
gcc -g -o0 -coverage -o a.out main.c
```

* 编译之后除了生成可执行文件还生成了`*.gcno`文件, **_gcno_** = gcov **_no_**tes

* `a.out`执行之后会成成`*.gcda`文件, **_gcda_** = gcov **_da_**ta

* 生成统计文件`*.gcov`
```bash
gcov main.c
```
* gcov内容格式说明
    * `#####`代表没有执行过的代码

## lcov简介
* [CSDN参考博客](https://blog.csdn.net/livelylittlefish/article/details/6321887)
* **_lcov_**是**_gcov_**的图形化的前端工具
* **_lcov_**编译安装
```bash
wget http://downloads.sourceforge.net/ltp/lcov-1.13.tar.gz
tar -xvf lcov-1.13.tar.gz
cd lcov-1.13
make install
```
<u>如果需要安装到自定义的目录, 修改**_Makefile_**的**_PREFIX_**路径即可</u>
* 捕获`*.gcda`中的信息并生成`*.info`文件
```bash
lcov --capture --directory . --output-file main.info --test-name a.out --no-external
# 增加--no-external可以去掉系统文件的统计
```

* 根据`*.info`输出html
```bash
genhtml main.info --output-directory html_output --title "simple test" --show-details --legend
```

* 到输出目录`html_output`打开`index.html`即可
