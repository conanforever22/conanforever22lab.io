---
title: 内存泄漏小工具valgrind
date: 2018-03-23
categories: [工具]
tags: [内存泄露]
---

下边的代码如果使用注释的内容的话就会发生内存泄漏，单独跑一次的话看不出来，如果在main函数外侧加上一个```while(true)```死循环就能够看到内存占用一直会上升，直到程序最终core掉

# 使用valgrind的方法如下：
```bash
valgrind --leak-check=full ./a.out
```

# 示例代码如下
```cpp
#include <iostream>
#include <cstring>

class A
{
public:
    A() { std::cout << "A()" << std::endl; memset(arr, 'a', 20*1024*1024);}
    virtual ~A(){ std::cout << "~A()" << std::endl;}
    char arr[200*1024*1024];
};

class B : public A
{
public:
    B() { std::cout << "B()" << std::endl; }
    virtual ~B(){ std::cout << "~B()" << std::endl;}
};

int main(void)
{
    //A *a = new A();
    A *a; 
    a = new B();
    delete a;
    a = NULL;

    return 0;
}
```
