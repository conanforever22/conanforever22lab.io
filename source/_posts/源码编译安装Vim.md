---
title: 源码编译安装Vim
date: 2018-05-08
categories: [工具安装]
tags: [Vim]
---

## 参考链接
[源码安装Vim](https://github.com/yangyangwithgnu/use_vim_as_ide#1)

## 为什么源码安装
有的Linux发行版预装的Vim版本太低, 一些新的比较好用的插件无法安装

## 使用下边的命令安装
注意修改安装路径和Python库的路径(vim编译安装需要python静态库`.a`)
```bash
git clone git@github.com:vim/vim.git
cd vim/
./configure --with-features=huge --enable-pythoninterp --enable-rubyinterp --enable-luainterp --enable-perlinterp --with-python-config-dir=/usr/lib/python2.7/config/ --enable-gui=gtk2 --enable-cscope --prefix=/usr
make
make install
```
