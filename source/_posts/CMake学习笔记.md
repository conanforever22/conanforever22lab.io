---
title: CMake学习笔记
date: 2018-04-24 12:36:24
categories: [工具]
tags: [编译]
---

## CMake安装
* 包管理器安装
```bash
sudo apt-get install cmake    # ubuntu
sudo yum install cmake3       # CentOS
```

* 源码编译安装
[CMake官网安装帮助文档](https://cmake.org/install/)

```bash
wget https://cmake.org/files/v3.8/cmake-3.8.2.tar.gz
# https://cmake.org/download/ for latest version
tar -xvf cmake-3.8.2.tar.gz 
./bootstrap --prefix=/home/conanforever22/tools/runtime
make
make install
```

## Hello World
[Reference here](https://github.com/Akagi201/learning-cmake/blob/master/docs/cmake-practice.pdf)

* 目录结构
```bash
.
├── t1
│   ├── CMakeLists.txt
│   └── main.c
└── t1_build
```

* main.c内容
```c
// main.c
#include <stdio.h>

int main(void)
{
    printf("Hello World form t1 Main!\n");

    return 0;
}
```

* CMakeLists.txt内容
```cmake
PROJECT(HELLO)
SET(SRC_LIST main.c)
MESSAGE(STATUS "This is BINARY dir " ${HELLO_BINARY_DIR})
MESSAGE(STATUS "This is SOURCE dir " ${HELLO_SOURCE_DIR})
ADD_EXECUTABLE(hello ${SRC_LIST})
```

* 使用cmake进行构建
```bash
cd t1_build    # 使用cmake会生成很多中间文件，为了避免对代码文件造成影响，在新建的目录下进行构建
cmake ../t1    # 输出如下
-- The C compiler identification is GNU 4.4.7
-- The CXX compiler identification is GNU 4.4.7
-- Check for working C compiler: /usr/bin/cc
-- Check for working C compiler: /usr/bin/cc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Check for working CXX compiler: /usr/bin/c++
-- Check for working CXX compiler: /usr/bin/c++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- This is BINARY dir /home/conanforever22/masl/cmake_learn/t1_build
-- This is SOURCE dir /home/conanforever22/masl/cmake_learn/t1
-- Configuring done
-- Generating done
-- Build files have been written to: /home/conanforever22/masl/cmake_learn/t1_build
```

## Miscellaneous Tips
* 如何使用系统的环境变量
```cmake
$ENV{VAR_NAME_HERE}
```

* [如何判断系统编译器版本](https://stackoverflow.com/questions/28532444/cmake-string-token-inclusion-check)
```cmake
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    # something to do here
elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "AppleClang")
    # other things to do here
endif()
```
